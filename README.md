#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>

// 全局变量
int high ,width;//游戏画面大小
int bird_x,bird_y;//小鸟坐标
int bar_y,bar_xTop,bar_xDown;//障碍物坐标
int score ;//得分




void gotoxy(int x,int y)  //光标移动到(x,y)位置
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle,pos);
}

void HideCursor() // 用于隐藏光标
{
    CONSOLE_CURSOR_INFO cursor_info = {1, 0};  // 第二个值为0表示隐藏光标
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}


void startup() // 数据初始化
{
    high = 15;
    width = 25;
    
    bird_x=1;
    bird_y=width /3;
    
    bar_y=width;
    bar_xTop=high/4;
    bar_xDown=high/2;

    score=0;
   
 
    HideCursor(); // 隐藏光标
}

void show()  // 显示画面
{
    gotoxy(0,0);  // 光标移动到原点位置，以下重画清屏
    int i,j;
    for (i=0;i<=high+1;i++)
    {
        for (j=0;j<=width;j++)
        {
            if (i==bird_x&&j==bird_y)
                printf("@");  //输出小鸟
            else if (j==bar_y&&(i<=bar_xTop||i>=bar_xDown))
               printf("*");//输出障碍物
            else
                printf(" ");  //   输出空格
        }
        printf("\n");
    }
       printf("得分：%d\n",score);
}    

void updateWithoutInput()  // 与用户输入无关的更新
{
    if (bird_y==bar_y)
    {
        if (bird_x>bar_xTop&&bird_x<bar_xDown)
            score++;
        else
        {
            printf("Game Over!\n");
            exit(0);
        }
    }


    bird_x++;
    if (bar_y>0)
        bar_y--;
    else
    {
        bar_y=width;

        int randPosition=rand()%(high-5);

        bar_xTop=randPosition;
        bar_xDown=randPosition+high/4;
    }
    Sleep(200);
}

void updateWithInput()  // 与用户输入有关的更新
{
    char input;
    if(kbhit())  // 判断是否有输入
    {
        input = getch();  // 根据用户的不同输入来移动，不必输入回车
        if (input == ' ')   
        {
            bird_x=bird_x-2;
        }



       
        if(input==27)
        {
        for(;;)
        {
                if(getch()==27)
                    break;
        }
        
        }
        
    }
}

int main()
{
    startup();  // 数据初始化    
    while (1) //  游戏循环执行
    {
        show();  // 显示画面
        updateWithoutInput();  // 与用户输入无关的更新
        updateWithInput();  // 与用户输入有关的更新
    }
    return 0;
}